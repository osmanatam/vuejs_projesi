FROM node:10

WORKDIR /app

COPY package*.json ./

RUN npm install
RUN npm install -g serve

COPY . .

EXPOSE 80

CMD [ "serve", "-l", "80" ]